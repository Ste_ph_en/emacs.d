;;; init-cpp.el -- google style C++
;;; Commentary:
;;; Code:

(setq c-default-style "linux")

(setq c-basic-offset 4)

(require-package 'cc-mode)

;;; google style
(when  (require-package 'google-c-style)
  (add-hook 'c-mode-common-hook 'google-set-c-style))



(provide 'init-cpp)
;;; init-cpp.el ends here
