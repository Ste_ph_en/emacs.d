;;; init-yasnippet.el --- snippet plugin manager
;;; Commentary:
;;; Code:


(add-to-list 'load-path
             "~/.emacs.d/yasnippet")
(require-package 'yasnippet)
(yas-global-mode 1)

(require-package 'yasnippet-snippets)

(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))

(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
