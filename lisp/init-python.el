;;; init-python.el --- Python editing -*- lexical-binding: t -*-
;;; Commentary:

;;; M-x elpy-config to install black auto formater and other optional featurs

;;; Code:

;; See the following note about how I set up python + virtualenv to
;; work seamlessly with Emacs:
;; https://gist.github.com/purcell/81f76c50a42eee710dcfc9a14bfc7240




(setq auto-mode-alist
      (append '(("SConstruct\\'" . python-mode)
                ("SConscript\\'" . python-mode))
              auto-mode-alist))

(setq python-shell-interpreter "/usr/local/anaconda3/bin/python")
;; (setq python-shell-virtualenv-path "/path/to/anaconda/envs/envname")

(require-package 'pip-requirements)



;;; most python IDE features
;;;  elpy ships as one big package that can make emacs sluggish
;;;  if you are having performacnce issuses try purcells implementation of anaconda-mode
;;; https://github.com/purcell/emacs.d/blob/master/lisp/init-python.el

(require-package 'elpy )
(require-package 'blacken) ;; auto formater automatically recognized by elpy

(with-eval-after-load 'python
  (elpy-enable)
  ;; Enable Flycheck
  (when (require-package 'flycheck nil t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode)))


(provide 'init-python)
;;; init-python.el ends here
