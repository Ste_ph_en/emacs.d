;;; init-multi-term.el --- prefered terminal
;;; Commentary:
;;; Code:


(require-package 'multi-term)
(global-set-key (kbd "C-x t") 'multi-term-dedicated-toggle)
(global-set-key (kbd "C-x T") 'multi-term)


(add-hook 'term-mode-hook (lambda ()
                            (setq term-buffer-maximum-size 1000
                                  term-scroll-to-bottom-on-output t
                                  multi-term-scroll-show-maximum-output t
                                  multi-term-dedicated-select-after-open-p t
                                  mode-line-format nil)
                            (toggle-truncate-lines 1)

                            (define-key term-raw-map (kbd "C-t") 'my-term-switch-line-char)
                            (define-key term-mode-map (kbd "C-t") 'my-term-switch-line-char)
                            (define-key term-raw-map (kbd "M-x") 'execute-extended-command)
                            (define-key term-raw-map (kbd "C-y") 'term-paste)
                            (define-key term-raw-map (kbd "C-c C-e") 'term-send-esc)
                            ))



(defun my-term-switch-line-char ()
  "Switch `term-in-line-mode' and `term-in-char-mode' in `ansi-term'"
  (interactive)
  (cond
   ((term-in-line-mode)
    (term-char-mode)
    (hl-line-mode -1))
   ((term-in-char-mode)
    (term-line-mode)
    (hl-line-mode 1))))
;; will temporarily make Emacs believes that there is no active process when you kill it,
;; and therefore you won't get the annoying prompt.

(require 'cl-lib)
(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  "Prevent annoying \"Active processes exist\" query when you quit Emacs."
  (cl-letf (((symbol-function #'process-list) (lambda ())))
    ad-do-it))

(provide 'init-multi-term)
;;; init-multi-term.el ends here
