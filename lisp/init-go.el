;;; init-go.el --- Go editing
;;; Commentary:
;;; Code:
;; Snag the user's PATH and GOPATH

;; -*- no-byte-compile: t; -*-
;;
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
"
setup
export GOPATH=$HOME/code/go
go get -u github.com/stamblerre/gocode
go get golang.org/x/tools/cmd/guru
go get -u github.com/motemen/gore
go get golang.org/x/tools/cmd/goimports
go get github.com/rogpeppe/godef
go get golang.org/x/tools/cmd/gorename
"
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


;;(require-package 'flymake-go)
;; (require-package 'gorepl-mode)
(setq lsp-keymap-prefix (kbd "C-?"))
(add-hook 'go-mode-hook #'lsp-deferred)
(require-package 'lsp-mode)
(when (maybe-require-package 'lsp-ui)
  (add-hook 'go-mode-hook 'lsp-ui-mode))

(require-package 'go-mode)

(setq gofmt-command "goimports")                ; gofmt uses invokes goimports

(when (maybe-require-package 'company-go)
  (after-load 'company
    (after-load 'go-mode
      (push 'company-go company-backends))))


;; Define function to call when go-mode loads
(defun sanityinc/go-hook ()
  "Attempt to set my go config."
  (add-hook 'before-save-hook 'gofmt-before-save) ;  gofmt before every save
  (if (not (string-match "go" compile-command)) ; set compile command default
      (set (make-local-variable 'compile-command)
           "go build -v && go test -v -cover && go vet && golint"))
  )

;; Key bindings specific to go-mode
;; (after-load 'go-mode
;;   )

(eval-after-load 'go-mode
  '(progn
     (define-key go-mode-map (kbd "M-.") 'godef-jump) ; Go to definition
     (define-key go-mode-map (kbd "M-,") 'pop-tag-mark) ; Return from whence you came
     (define-key go-mode-map (kbd "C-c C-c") 'recompile) ; Invoke compiler
     (define-key go-mode-map (kbd "C-c C-t") 'go-run-all-tests) ;
     ;;      (define-key go-mode-map (kbd "C-c M-?") 'go-guru-referrers)
     ))

(defun go-play ()
  "Create a simple script in tmp directroy to test small things."
  (interactive)
  (let* ((temporary-file-directory (expand-file-name "tmp/" (getenv "GOPATH")))
         (temp-file (progn
                      (make-directory temporary-file-directory t)
                      (make-temp-file "go-play" nil ".go"))))
    (find-file temp-file)
    (insert "package main
import \"fmt\"
func main() {
    fmt.Printf(\"\")
}")
    (goto-char 55)
    (go-mode)
    (define-key (current-local-map) (kbd "C-c C-k")
      (lambda () (interactive) (save-buffer) (delete-file buffer-file-name) (kill-buffer)))
    (define-key (current-local-map) (kbd "C-c C-c")
      (lambda () (interactive) (save-buffer) (compile (format "go run %s" buffer-file-name))))))


;; Connect go-mode-hook with the function we just defined
(add-hook 'go-mode-hook 'sanityinc/go-hook)

(provide 'init-go)
;;; init-go.el ends here
