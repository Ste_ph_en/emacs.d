;;; init.el --- Load the full configuration -*- lexical-binding: t -*-
;;; Commentary:

;; This file bootstraps the configuration, which is divided into
;; a number of other files.

;;; Code:

;; Produce backtraces when errors occur: can be helpful to diagnose startup issues
;;(setq debug-on-error t)

(let ((minver "24.4"))
  (when (version< emacs-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))
(when (version< emacs-version "25.1")
  (message "Your Emacs is old, and some functionality in this config will be disabled. Please upgrade if possible."))

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(require 'init-benchmarking) ;; Measure startup time

(defconst *spell-check-support-enabled* nil) ;; Enable with t if you prefer
(defconst *is-linux* (eq system-type 'gnu/linux))

;;----------------------------------------------------------------------------
;; Adjust garbage collection thresholds during startup, and thereafter
;;----------------------------------------------------------------------------
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;;----------------------------------------------------------------------------
;; Bootstrap config
;;----------------------------------------------------------------------------
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(require 'init-utils)
;; (require 'init-site-lisp)
;;(require 'init-site-lisp) ;; Must come before elpa, as it may provide package.el
;; Calls (package-initialize)
(require 'init-elpa)      ;; Machinery for installing required packages
;; (require 'init-exec-path) ;; Set up $PATH

;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-preload-local.el"
;;----------------------------------------------------------------------------
;; (require 'init-preload-local nil t)
;; don't need to inlcude if not going to use feature.
;;----------------------------------------------------------------------------
;; Load configs for specific features and modes
;;----------------------------------------------------------------------------

(require-package 'diminish)             ;hides active minor modes from bar
(maybe-require-package 'scratch)        ;create a scratch buffer with the same major mode as the current buffer’s.
(require-package 'command-log-mode)     ;Show event history and command history of some or all buffers?

(require 'init-frame-hooks)             ;functions to hook code to GUT/TTY frame creation: utility
(require 'init-xterm)                   ;xterm (use mouse in terminal)
(require 'init-themes)                  ;set default light dark themes, and dimmer for non-active frames
(require 'init-gui-frames)              ;crtl-z, adjust-opacity, gui-features, opacity, font size, disable-mouse
(require 'init-dired)                   ;dir preferences
(require 'init-isearch)                 ;addional isearch preferences
(require 'init-grep)                    ;grep\find "M-?"
(require 'init-uniquify)                ;Nicer naming of buffers for files with identical names
(require 'init-ibuffer)                 ;pretty switch buffer with "C-x C-b"
(require 'init-flycheck)                ;visual colored indecations of format errors
(require 'init-multi-term)              ;C-c t lanches terminal TODO: in new window

(require 'init-recentf)                 ;tracking reccent files
(require 'init-smex)                    ;improve M-x by tracking recent commands
(require 'init-ivy)                     ;flexable search for M-x, enable with sanityinc/enable-ivy-flx-matching and search swiper
;;; also has swiper search M-s / and

(require 'init-hippie-expand)           ;M-/ function, var,  work lookup
(require 'init-company)                 ;M-/ Completion with company
(require 'init-windows)                 ;improvments to spliting windows
(require 'init-sessions)                ;desktop save session config
(require 'init-mmm)                     ;Multiple Major Modes support

(require 'init-editing-utils)           ;most editing preferences including multi-cursor
(require 'init-whitespace)              ;Special handling for whitespace


(require 'init-vc)                      ;load diff-hl highlight git changes and vc-diff to merge changes
(require 'init-darcs)                   ;Support for the Darcs DVCS
(require 'init-git)
(require 'init-github)

(require 'init-projectile)

(require 'init-compile)
(require 'init-textile)
(require 'init-markdown) ; edit .md style files
(require 'init-csv)
(require 'init-org)
(require 'init-nxml)
(require 'init-html)
(require 'init-css)
(require 'init-http)
(require 'init-python)
(require 'init-sql)
(require 'init-yaml)
(require 'init-docker)
(require 'init-go)
(require 'init-yasnippet)
(require 'init-cpp)
(maybe-require-package 'nginx-mode) ;idk what this is

(require 'init-paredit)                 ; bracket completion
(require 'init-lisp)
(require 'init-slime)
(require 'init-clojure)
(require 'init-clojure-cider)
(require 'init-common-lisp)

(when *spell-check-support-enabled*
  (require 'init-spelling))

(require 'init-misc)

(require 'init-folding)

(require 'init-ledger)


;;----------------------------------------------------------------------------
;; Extra packages which don't require any configuration
;;----------------------------------------------------------------------------

(require-package 'gnuplot)
(require-package 'lua-mode)
(require-package 'htmlize)
(require-package 'dsvn)
(maybe-require-package 'daemons)



(when (maybe-require-package 'uptimes)
  (setq-default uptimes-keep-count 200)
  (add-hook 'after-init-hook (lambda () (require 'uptimes))))

(when (fboundp 'global-eldoc-mode)
  (add-hook 'after-init-hook 'global-eldoc-mode))

;;----------------------------------------------------------------------------
;; Allow access from emacsclient
;;----------------------------------------------------------------------------
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))

;;----------------------------------------------------------------------------
;; Variables configured via the interactive 'customize' interface
;;----------------------------------------------------------------------------
(when (file-exists-p custom-file)
  (load custom-file))


;;----------------------------------------------------------------------------
;; Locales (setting them earlier in this file doesn't work in X)
;;----------------------------------------------------------------------------
(require 'init-locales)


;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-local" containing personal settings
;;----------------------------------------------------------------------------
(require 'init-local nil t)



(provide 'init)

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
;;; init.el ends here
(put 'list-timers 'disabled nil)
